/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author a
 */
public class RectangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblWide = new JLabel("Wide:", JLabel.TRAILING);
        lblWide.setSize(50, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        frame.add(lblWide);

        JLabel lblLength = new JLabel("Length:", JLabel.TRAILING);
        lblLength.setSize(50, 20);
        lblLength.setLocation(5, 25);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        frame.add(lblLength);

        final JTextField txtWide = new JTextField();
        txtWide.setSize(50, 20);
        txtWide.setLocation(60, 5);
        frame.add(txtWide);

        final JTextField txtLength = new JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(60, 25);
        frame.add(txtLength);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 15);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle Wide = ??? Rectangle Length = ??? "
                + "area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.lightGray);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strLength = txtLength.getText();
                    String strWide = txtWide.getText();
                    double length = Double.parseDouble(strLength);
                    double wide = Double.parseDouble(strWide);
                    Rectangle rectangle = new Rectangle(wide, length);
                    lblResult.setText("Rectangle Wide = " + String.format("%.2f", rectangle.getWide())
                            + " Length = " + String.format("%.2f", rectangle.getLength())
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtLength.setText("");
                    txtLength.requestFocus();
                    txtWide.setText("");
                    txtWide.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}
