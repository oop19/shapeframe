/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author a
 */
public class TriangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblHigh = new JLabel("High:", JLabel.TRAILING);
        lblHigh.setSize(50, 20);
        lblHigh.setLocation(5, 5);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        frame.add(lblHigh);

        JLabel lblBase = new JLabel("Base:", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 25);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        final JTextField txtHigh = new JTextField();
        txtHigh.setSize(50, 20);
        txtHigh.setLocation(60, 5);
        frame.add(txtHigh);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 25);
        frame.add(txtBase);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 15);
        frame.add(btnCalculate);

         final JLabel lblResult = new JLabel("Rectangle Wide = ??? Rectangle Length = ??? "
                + "area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strHigh = txtHigh.getText();
                    String strBase = txtBase.getText();
                    double high = Double.parseDouble(strHigh);
                    double base = Double.parseDouble(strBase);
                    Triangle triangle = new Triangle(high, base);
                    lblResult.setText("Triangle High = " + String.format("%.2f", triangle.getHigh())
                            + " Base = " + String.format("%.2f", triangle.getBase())
                            + " Area = " + String.format("%.2f", triangle.calArea())
                            + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtHigh.setText("");
                    txtHigh.requestFocus();
                    txtBase.setText("");
                    txtBase.requestFocus();
                }
            }
        });
        
        frame.setVisible(true);
    }
}
