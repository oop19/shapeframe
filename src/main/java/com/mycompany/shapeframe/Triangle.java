/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeframe;

/**
 *
 * @author a
 */
public class Triangle extends Shape{
    private double high;
    private double base;
    private double c;
    
    public Triangle(double high , double base) {
        super("Triangle");
        this.high = high;
        this.base = base;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }
    
    

    @Override
    public double calArea() {
        return 0.5 * base * high;
    }

    @Override
    public double calPerimeter() {
       double c = Math.sqrt(Math.pow(base, 2) + Math.pow(high, 2));
       return c + base + high;
    }
    
}
